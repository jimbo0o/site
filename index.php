<?php require('header.php') ?>
<?php require('menu.php') ?>
<?php require('slider.php') ?>
<div class="content">
	<?php require('sidebar.php') ?>
	<div class="main"> 
		<?php 
			$str = mysqli_query($connect, "SELECT * FROM news ORDER BY id_news DESC LIMIT 1");
			$material = mysqli_fetch_array($str, MYSQLI_ASSOC);
	?>
			<h1 class="news-title"> <?php echo $material['title']; ?> </h1>
			<p> <?php echo $material['full_text'] ?> </p>
	</div>
</div>
<?php require('./footer.php') ?>
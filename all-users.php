<?php require('header.php') ?>
<?php require('menu.php') ?>
<div class="content">
	<?php require('sidebar.php'); ?>
	<div class="main"> 
		<?php require_once('connection.php'); ?>
		<h2> Пользователи </h2>
		<?php $str = mysqli_query($connect, "SELECT * FROM users"); ?>
		<table class="db_table">
			<?php while($user = mysqli_fetch_array($str)) { ?>
			<tr>
				<td> <?php echo $user['id_user']; ?> </td>
				<td> <?php echo $user['login']; ?> </td>
				<td> <?php echo $user['email']; ?> </td>
				<td> <style="color: black" a href="./user-edit.php?id=<?php echo $user['id_user']; ?>"> изменить </a> </td>
				<td> <a style="color: red" href="./act_delete_user.php?id=<?php echo $user['id_user']; ?>"> удалить </a> </td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>
<?php require('footer.php'); ?>
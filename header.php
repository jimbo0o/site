<!DOCTYPE html>
<html>
<head>
	<title> Футбольный клуб «Шахтер» </title>
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<?php 
		require_once('connection.php');
		session_start();
	?>
	<div class="user-panel">
		<ul> 
			<?php if(isset($_SESSION["id_user"])) { ?>
			<li class="dropdown"> 
  					<span style="color: white;"> <a href="./profile.php"> <?php echo $_SESSION["login"]; ?> </span> </a>
  						<div class="dropdown-content">
  							<p> Login: <?php echo $_SESSION["login"]; ?> </p>
    						<p> Email: <?php echo $_SESSION["email"]; ?> </p>
  					</div>
			</li>
			<li> <a href="all-users.php"> пользователи </a> </li>
			<li> <a href="all-news.php"> новости </a> </li>
			<li> <a href="all-fb.php"> обратная связь </a> </li>
			<li> <a href="act_logout.php"> выход </a> </li>
			<?php } else { ?>
			<li> <a href="./signup.php"> Регистрация </a> </li>
			<li> <button class="login-btn"> Войти в аккаунт </button> </li>
			<?php } ?>
		</ul> 
	</div>

	<div class="header">
		<p class="logo-desc"> Новостной сайт </p>
	</div>